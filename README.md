![logo](resources/logo.png)

---

# Welcome to a `Golang` workshop!
In this repository you will be able to find various prepared sessions, 
scoped to branches of this repository, that you can follow to add to your
`Go` learning journey!

This is project is based on the https://github.com/cdarwin/go-koans koans project
which already provides a well-thought-out range of exercises. I have simply extending 
this work with mine and added further tests that aim at diving deeper into some of the
more advanced gotchas when coding in Go.

## Sessions
Below you find a table containing all the sessions that have been crafted thus far. 
Feel free to contribute with your own! 🏆

| `Number` | `Link`                                                      |
|:--------:|:------------------------------------------------------------|
 |  **W1**  | [click me!](https://gitlab.com/pcanilho/go-koans/-/tree/W1) |
 |  **W2**  | [click me!](https://gitlab.com/pcanilho/go-koans/-/tree/W2) |

## Requirements
* `Go`: No specific version is required but having `1.19`+ installed is recommended.

## How-to
1. Clone this repository:

```shell
$ git clone git@gitlab.com:pcanilho/go-koans.git
```

2. Change directory to `go-koans` and fetch all dependencies (not required for all sessions)

```shell
$ cd go-koans && go get ./...
```

3. Start coding!

```shell
$ go test -v .
```
